import 'package:flutter/material.dart';
import 'package:test_work/presentation/pages/login_page/login_page.dart';
import 'package:test_work/presentation/pages/main_page/main_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // Логин - 123
  // Пароль - 123
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mobile REST Client',
      debugShowCheckedModeBanner: false,
      initialRoute: '/login',
      routes: {
        '/login': (context) => LoginPage(),
        '/main': (context) => MainPage()
      },
    );
  }
}

