import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_work/logic/bloc/fact_bloc/fact_bloc.dart';
import 'package:test_work/presentation/values/appColors.dart';

import 'body.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: lightBgColor,
        body: BlocProvider<FactBloc>(
          create: (context) => FactBloc(),
          child: MainBody(),
        ),
        appBar: AppBar(
          backgroundColor: lightBgColor,
          elevation: 0.0,
          actions: [
            TextButton(
                onPressed: () =>
                    Navigator.pushNamedAndRemoveUntil(
                        context, "/login", (Route<dynamic> route) => false),
                // Очищает весь стек страниц и откидывает до страницы логина
                child: Row(children: [
                  Text('Выйти', style: TextStyle(color: Colors.black, fontSize: 18.0)),
                  SizedBox(width: 5.0),
                  Icon(Icons.exit_to_app, color: Colors.black, size: 24)
                ]))
          ],
        ));
  }
}
