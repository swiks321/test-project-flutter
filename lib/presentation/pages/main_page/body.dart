import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_work/logic/bloc/fact_bloc/fact_bloc.dart';
import 'package:test_work/presentation/pages/info_page/info_page.dart';
import 'package:test_work/presentation/values/appColors.dart';

class MainBody extends StatelessWidget {
  const MainBody({Key key}) : super(key: key);

  /// Так как у нас в любом FactState всегда находятся факты -
  /// предыдущие или новые - то проверку моржно делать только на стейт загрузки
  @override
  Widget build(BuildContext context) {
    FactBloc _factBloc = BlocProvider.of<FactBloc>(context);
    _factBloc.add(FactLoadMoreEvent());

    // Реализация бесконечной подгрузки фактов
    ScrollController _controller = ScrollController();
    _controller.addListener(() {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels != 0) {
          _factBloc.add(FactLoadMoreEvent());
        }
      }
    });

    return BlocBuilder<FactBloc, FactState>(
      builder: (context, state) {
        return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            controller: _controller,
            itemCount: state.facts.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => InfoPage(fact: state.facts[index]))),
                child: Container(
                  margin: EdgeInsets.all(8.0),
                  child: Card(
                    elevation: 3.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0)),
                    color: darkBgColor,
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 14.0, horizontal: 13.0),
                        child: IntrinsicHeight( // Что бы Divider отображались правльно
                          child: Row(
                            children: [
                              SizedBox(width: 21.0),
                              Text(
                                '#${index + 1}',
                                style: TextStyle(
                                    fontSize: 20.0,
                                    color: primaryTextColor,
                                    fontWeight: FontWeight.w300),
                              ),
                              SizedBox(width: 15.0),
                              VerticalDivider(width: 1, thickness: 0.7, color: darkDividerColor),
                              SizedBox(width: 15.0),
                              Expanded(
                                  child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    state.facts[index].verified
                                        ? 'Verified'
                                        : 'Unverified',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w300,
                                        fontSize: 20.0,
                                        color: primaryTextColor),
                                  ),
                                  SizedBox(height: 6.0),
                                  Text(
                                    state.facts[index].text,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: 12.0,
                                      color: primaryTextColor,
                                    ),
                                    textAlign: TextAlign.justify,
                                  )
                                ],
                              )),
                              SizedBox(
                                width: 24.0,
                              )
                            ],
                          ),
                        )),
                  ),
                ),
              );
            });
      },
    );
  }
}
