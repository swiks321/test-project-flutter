import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_work/logic/bloc/user_bloc/user_bloc.dart';
import 'package:test_work/presentation/pages/login_page/body.dart';
import 'package:test_work/presentation/values/appColors.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: darkBgColor,
        body: BlocProvider<UserBloc>(
          create: (context) => UserBloc(),
          child: LoginBody(),
        ));
  }
}
