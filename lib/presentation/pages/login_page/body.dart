import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_work/logic/bloc/user_bloc/user_bloc.dart';
import 'package:test_work/presentation/values/appColors.dart';

import 'components/text_field_decoration.dart';

class LoginBody extends StatelessWidget {
  const LoginBody({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController _loginController = TextEditingController();
    TextEditingController _passwordController = TextEditingController();

    UserBloc _userBloc = BlocProvider.of<UserBloc>(context);

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: 1),
        Text('Авторизация',
            style: TextStyle(fontSize: 36.0, color: primaryTextColor)),
        Padding(
          padding: const EdgeInsets.only(left: 25.0, right: 29.0),
          child: Column(
            children: [
              TextField(
                decoration: getDefaultTextFieldDecoration(hint: 'Логин'),
                controller: _loginController,
                style: TextStyle(color: secondaryTextColor),
                cursorColor: secondaryTextColor,
              ),
              SizedBox(height: 20),
              TextField(
                decoration: getDefaultTextFieldDecoration(hint: 'Пароль'),
                controller: _passwordController,
                style: TextStyle(color: secondaryTextColor),
                cursorColor: secondaryTextColor,
              )
            ],
          ),
        ),
        SizedBox(height: 1),
        BlocConsumer<UserBloc, UserState>(
            listener: (context, state) {
          if (state is UserCorrectAuthState) {
            Navigator.pushNamed(context, '/main');
          }
        }, builder: (context, state) {
          if (state is UserCheckingAuthState) {
            return Container(
                padding: const EdgeInsets.all(10),
                margin: const EdgeInsets.only(bottom: 39.0),
                child: SizedBox(
                  height: 61,
                  width: 61,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                ));
          }
          return Container(
              margin: const EdgeInsets.only(bottom: 39.0),
              decoration: BoxDecoration(color: primaryBtnColor, boxShadow: [
                BoxShadow(
                    color: Colors.black.withOpacity(0.25),
                    blurRadius: 10.0,
                    offset: Offset(0.0, 4.0))
              ]),
              child: TextButton(
                  onPressed: () => _userBloc.add(UserTryAuthEvent(
                      login: _loginController.text,
                      password: _passwordController.text)),
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 20.0, horizontal: 91.0),
                    child: Text(
                      state is UserIncorrectAuthState
                          ? 'ПОПРОБУЙТЕ ЕЩЁ'
                          : 'ВОЙТИ',
                      style: TextStyle(
                        color: primaryTextColor,
                        fontSize: 21.0,
                      ),
                    ),
                  )));
        })
      ],
    );
  }
}
