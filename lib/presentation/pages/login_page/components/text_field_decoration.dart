import 'package:flutter/material.dart';
import 'package:test_work/presentation/values/appColors.dart';

InputDecoration getDefaultTextFieldDecoration({String hint}) {
  return InputDecoration(
      hintText: hint,
      hoverColor: secondaryTextColor,
      fillColor: secondaryTextColor,
      focusColor: secondaryTextColor,
      hintStyle: TextStyle(fontSize: 16.0, color: secondaryTextColor),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: Colors.white,
        ),
      ),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: secondaryTextColor,
        ),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(
          color: secondaryTextColor,
        ),
      ));
}
