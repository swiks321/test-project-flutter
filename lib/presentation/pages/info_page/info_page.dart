import 'package:flutter/material.dart';
import 'package:test_work/data/models/fact_model.dart';
import 'package:test_work/presentation/values/appColors.dart';

import 'body.dart';

class InfoPage extends StatelessWidget {
  final fact;
  const InfoPage({Key key, @required this.fact}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: lightBgColor,
        body: InfoBody(fact: fact),
        appBar: AppBar(
          automaticallyImplyLeading: false, // Убираем стрелку "назад"
          backgroundColor: lightBgColor,
          elevation: 0.0, // Убираем тень
          actions: [
            TextButton(
                onPressed: () => Navigator.pushNamedAndRemoveUntil(context, "/login", (Route<dynamic> route) => false),
                child: Row(children: [
                  Text('Выйти', style: TextStyle(color: Colors.black, fontSize: 18.0)),
                  SizedBox(width: 5.0),
                  Icon(Icons.exit_to_app, color: Colors.black, size: 24)
                ]))
          ],
        ));
  }
}
