import 'package:flutter/material.dart';
import 'package:test_work/data/models/fact_model.dart';
import 'package:test_work/presentation/values/appColors.dart';

class InfoBody extends StatelessWidget {
  final Fact fact;
  const InfoBody({Key key, @required this.fact}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 27.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image(image: AssetImage('assets/images/cat.png'), width: 200),
          Divider(height: 1, thickness: 1, color: darkDividerColor),
          Text(fact.verified ? 'Verified' : 'Unverified', style: TextStyle(fontSize: 24.0),),
          Text(fact.text, style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w300, color: darkTextColor)),
          Divider(height: 1, thickness: 1, color: darkDividerColor),
          Container(
            margin: const EdgeInsets.only(bottom: 33.0),
            decoration: BoxDecoration(color: primaryBtnColor, boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.25),
                  blurRadius: 10.0,
                  offset: Offset(0.0, 4.0))
            ]),
            child: TextButton(
                onPressed: () => Navigator.pop(context),
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 91.0),
                  child: Text(
                    'Назад',
                    style: TextStyle(
                      color: primaryTextColor,
                      fontSize: 16.0,
                    ),
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
