import 'package:flutter/material.dart';

const Color primaryTextColor = Colors.white;
const Color secondaryTextColor = Color.fromARGB(255, 242, 242, 242);
const Color darkTextColor = Color.fromARGB(255, 58, 58, 58);
const Color primaryBtnColor = Color.fromARGB(255, 51, 51, 51);

const Color darkBgColor = Color.fromARGB(255, 102, 87, 161);
const Color lightBgColor = Colors.white;

const Color dividerColor = Color.fromARGB(255, 194, 194, 194);
const Color darkDividerColor = Color.fromARGB(255, 224, 224, 224);