import 'package:test_work/data/models/fact_model.dart';
import 'package:test_work/data/providers/fact_provider.dart';

class FactRepository {
  FactProvider _factProvider = FactProvider();

  Future <List<Fact>> getNewFacts() async {
    return _factProvider.getFacts();
  }
}