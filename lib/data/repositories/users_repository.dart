import 'package:test_work/data/models/user_model.dart';
import 'package:test_work/data/providers/users_provider.dart';

class UsersRepository {
  UsersProvider _usersProvider = UsersProvider();

  Future<List<User>> getAllUsers() async {
    return _usersProvider.getAllUsers();
  }
}