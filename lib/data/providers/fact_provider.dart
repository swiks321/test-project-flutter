import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:test_work/data/models/fact_model.dart';

class FactProvider {
  final String url = 'https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=10';

  Future<List<Fact>> getFacts() async {
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final List body = json.decode(response.body);
      return body.map((e) => Fact.fromJson(e)).toList();
    } else {
      return [];
    }
  }

}