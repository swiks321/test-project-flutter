import 'package:test_work/data/models/user_model.dart';

class UsersProvider {
  // Типо получаем данные с сервера, например с Firestore
  Future<List<User>> getAllUsers() async {
    return await Future.delayed(Duration(seconds: 2), () {
      return [User(login: '123', password: '123'),
              User(login: '111', password: '111')];
    });
  }
}