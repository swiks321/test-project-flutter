import 'package:flutter/cupertino.dart';

class Fact {
  String text;
  bool verified;

  Fact({@required this.text, @required this.verified});

  factory Fact.fromJson(Map<String, dynamic> json) {
    return Fact(
        text: json['text'],
        verified: json['status']['verified'] ?? false
    );
  }

}