import 'dart:convert';

import 'package:flutter/foundation.dart';


class User {
  String _token;

  get token => _token;

  User({@required String login, @required String password}) {
    this._token = utf8.encode(login + password).join();
    // Нереальная система шифрования данных
  }
}