part of 'user_bloc.dart';

@immutable
abstract class UserEvent {}

class UserTryAuthEvent extends UserEvent {
  final String login;
  final String password;

  UserTryAuthEvent({this.login, this.password});
}


