import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:test_work/data/models/user_model.dart';
import 'package:test_work/data/repositories/users_repository.dart';

part 'user_event.dart';

part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(UserInitial());
  UsersRepository _usersRepository = UsersRepository();

  // Проверяем токен на совпадение среди всех пользователей
  Future<bool> isValidUser(User user) async {
    for (final other in await _usersRepository.getAllUsers()) {
      if (other.token == user.token) {
        return true;
      }
    }
    return false;
  }

  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    if (event is UserTryAuthEvent) {
      yield UserCheckingAuthState();
      bool isValid =
          await isValidUser(User(login: event.login, password: event.password));
      if (isValid)
        yield UserCorrectAuthState();
      else
        yield UserIncorrectAuthState();
    }
  }
}
