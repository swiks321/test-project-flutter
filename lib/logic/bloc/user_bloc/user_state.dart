part of 'user_bloc.dart';

@immutable
abstract class UserState {}

class UserInitial extends UserState {}

class UserCheckingAuthState extends UserState {}

class UserCorrectAuthState extends UserState {}

class UserIncorrectAuthState extends UserState {}
