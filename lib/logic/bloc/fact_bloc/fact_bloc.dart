import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:test_work/data/models/fact_model.dart';
import 'package:test_work/data/repositories/fact_repository.dart';

part 'fact_event.dart';
part 'fact_state.dart';

class FactBloc extends Bloc<FactEvent, FactState> {
  FactBloc() : super(FactLoadedState(facts: <Fact>[]));

  List<Fact> _facts = [];
  FactRepository _factRepository = FactRepository();

  @override
  Stream<FactState> mapEventToState(
    FactEvent event,
  ) async* {
    if (event is FactLoadMoreEvent) {
      var _newFacts = await _factRepository.getNewFacts();
      _facts += _newFacts;
      yield FactLoadedState(facts: _facts);
    }
  }
}
