part of 'fact_bloc.dart';

@immutable
abstract class FactState {
  final List<Fact> facts;

  FactState(this.facts);
}

class FactLoadedState extends FactState {
  FactLoadedState({facts}) : super(facts);
}
